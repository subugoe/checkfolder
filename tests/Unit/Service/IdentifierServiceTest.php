<?php

namespace App\Tests\Unit\Service;

use App\Service\IdentifierService;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Finder;

#[CoversClass(IdentifierService::class)]
class IdentifierServiceTest extends TestCase
{
    private IdentifierService $fixture;

    #[\Override]
    protected function setUp(): void
    {
        $this->fixture = new IdentifierService();
    }

    #[Test]
    public function testDeterminingTheIdentifierWorks()
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../Fixtures');
        foreach ($finder as $file) {
            $this->assertSame('PPN871495430', $this->fixture->getIdentifier($file->getContents()));
        }
    }
}
