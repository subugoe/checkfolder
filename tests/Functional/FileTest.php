<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Service\CopyService;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

#[CoversClass(CopyService::class)]
class FileTest extends TestCase
{
    private CopyService $copyService;

    private string $sourceDirectory;
    private Filesystem $sourceFilesystem;

    private string $targetDirectory;

    private Filesystem $targetFilesystem;

    #[\Override]
    protected function setUp(): void
    {
        $this->sourceDirectory = __DIR__.'/../../var/source';
        $this->targetDirectory = __DIR__.'/../../var/target';

        $sourceAdapter = new LocalFilesystemAdapter($this->sourceDirectory);
        $targetAdapter = new LocalFilesystemAdapter($this->targetDirectory);

        $this->sourceFilesystem = new Filesystem($sourceAdapter);
        $this->targetFilesystem = new Filesystem($targetAdapter);

        /** @var EventDispatcher $eventDispatcher */
        $eventDispatcher = $this->getMockBuilder(EventDispatcher::class)->getMock();
        /** @var LoggerInterface $logger */
        $logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $this->copyService = new CopyService($this->targetFilesystem, $this->sourceFilesystem, $logger, $eventDispatcher);
    }

    /**
     * @throws \League\Flysystem\FilesystemException
     */
    #[\Override]
    protected function tearDown(): void
    {
        $this->targetFilesystem->deleteDirectory('/mets');
        $this->targetFilesystem->deleteDirectory('/orig');
    }

    public static function metsDataProvider(): array
    {
        return [
            ['PPN871495430'],
        ];
    }

    /**
     * @throws \League\Flysystem\FilesystemException
     */
    #[\PHPUnit\Framework\Attributes\Test]
    #[\PHPUnit\Framework\Attributes\DataProvider('metsDataProvider')]
    public function testAssertion(string $work)
    {
        // create directory structure
        $this->sourceFilesystem->createDirectory($work);
        $this->sourceFilesystem->write(sprintf('%s/%s.xml', $work, $work), file_get_contents(__DIR__.'/../Fixtures/'.$work.'.xml'));

        for ($i = 0; $i < 2; ++$i) {
            $image = file_get_contents(__DIR__.'/../img/Paris.jpg');

            $this->sourceFilesystem->write(sprintf('%s/%s_tif/%d.jpg', $work, $work, $i), $image);
        }

        $this->copyService->process();

        $this->assertFileExists($this->targetDirectory.'/mets/'.$work.'.xml');
        $this->assertFileExists($this->targetDirectory.'/orig/'.$work.'/0.jpg');
    }
}
