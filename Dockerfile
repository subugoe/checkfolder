FROM php:8.3-cli-alpine

ENV COMPOSER_ALLOW_SUPERUSER=1
ENV APP_ENV=prod

COPY . /usr/src/checkfolder
WORKDIR /usr/src/checkfolder
COPY --from=composer/composer:2-bin /composer /usr/local/bin/

RUN apk update && \
    apk add --no-cache \
        git \
        zlib-dev \
        libzip-dev \
        zip \
        unzip \
        icu-dev && \
    docker-php-ext-install zip  && \
    docker-php-ext-configure intl && docker-php-ext-install intl && \
    # Add PHP parameters and install all dependencies
    echo "memory_limit=2048M" > /usr/local/etc/php/conf.d/memory-limit.ini && \
    composer install --prefer-dist --no-progress --no-scripts --optimize-autoloader --classmap-authoritative  --no-interaction && \
    composer clear-cache && \
    rm -rf /usr/src/php && \
    rm -rf /var/cache/apk/*

CMD ["php", "./bin/console", "app:check-new-files"]
