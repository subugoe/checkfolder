<?php

declare(strict_types=1);

return Rector\Config\RectorConfig::configure()
    ->withSets([
        Rector\Symfony\Set\SymfonySetList::SYMFONY_72,
    ])
    ->withAttributesSets(symfony: true, doctrine: true, phpunit: true, sensiolabs: true)
    ->withPaths([
        'src/',
        'tests/',
    ])
    ->withPreparedSets(codingStyle: true, codeQuality: true)
    ->withPhpSets()
    ->withSkip([
        '*/vendor/*',
        '*/var/*',
    ]);
