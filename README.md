# Checkfolder 

This is the successor of the hotfolder application. It is primarily meant to be the bridge between Goobi
and the indexing mechanism for the GDZ website.

The tasks are:

1. Check if there are new documents (Mets / XML files)
2. Copy these files from the Goobi export folder to the S3 storage
3. Copy all related images and ocr files to the S3 storage
4. Delete all copied files from the Goobi storage
5. Notify the indexer API about new documents

## Installation

Either use a pre-built docker image or install the application with
`composer install` and add all desired environment parameters, such as S3 storage and secret key.

## Configuration

Fill the environment variables from the `.env`-file.

## Upgrading

The indexer path - in addition to the `INDEXER_DOMAIN` environment variable is a current addition, to make that more configurable.

`INDEXER_PATH=/api/indexer/jobs`
