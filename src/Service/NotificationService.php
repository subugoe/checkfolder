<?php

namespace App\Service;

use GuzzleHttp\Client;

/**
 * Send notification about new works to api.
 */
final class NotificationService implements NotificationInterface
{
    private readonly Client $client;

    public function __construct(string $indexerEndpoint, private readonly string $serviceContext, private readonly string $indexerPath, public string $serviceProduct)
    {
        $this->client = new Client(['base_uri' => $indexerEndpoint]);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    #[\Override]
    public function informAboutNewWork(string $id): bool
    {
        $requestObject = new \stdClass();

        $requestObject->document = $id;
        $requestObject->context = $this->serviceContext;
        // This is necessary because the indexer needs to work for different projects.
        $requestObject->product = $this->serviceProduct;

        $response = $this->client->post($this->indexerPath, ['json' => $requestObject]);

        return 200 === $response->getStatusCode();
    }
}
