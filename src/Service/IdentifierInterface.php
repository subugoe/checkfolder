<?php

namespace App\Service;

interface IdentifierInterface
{
    /**
     * Get the identifier from a given XML file.
     */
    public static function getIdentifier(string $xml): string;
}
