<?php

namespace App\Service;

/**
 * Send notification about new works to api.
 */
interface NotificationInterface
{
    public function informAboutNewWork(string $id): bool;
}
