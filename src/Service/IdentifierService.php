<?php

namespace App\Service;

use Symfony\Component\DomCrawler\Crawler;

final class IdentifierService implements IdentifierInterface
{
    private static array $xpaths = [
        '//mets:mets/mets:dmdSec/mets:mdWrap[@MDTYPE="MODS"]/mets:xmlData/mods:mods[1]/mods:identifier[@type="gbv-ppn"]',
        '//mets:mets/mets:dmdSec/mets:mdWrap[@MDTYPE="MODS"]/mets:xmlData/mods:mods[1]/mods:recordInfo/mods:recordIdentifier[@source="gbv-ppn"]',
        '//mets:mets/mets:dmdSec/mets:mdWrap[@MDTYPE="MODS"]/mets:xmlData/mods:mods[1]/mods:identifier[@type="ppn" or @type="PPN"]',
        '//mets:mets/mets:dmdSec/mets:mdWrap[@MDTYPE="MODS"]/mets:xmlData/mods:mods[1]/mods:identifier[@type="urn" or @type="URN"]',
        '//mets:mets/mets:dmdSec/mets:mdWrap[@MDTYPE="MODS"]/mets:xmlData/mods:mods[1]/mods:recordInfo/mods:recordIdentifier[@source="DE-611"]',
        '//mets:mets/mets:dmdSec/mets:mdWrap[@MDTYPE="MODS"]/mets:xmlData/mods:mods[1]/mods:recordInfo/mods:recordIdentifier[@source="Kalliope"]',
        '//mets:mets/mets:dmdSec/mets:mdWrap[@MDTYPE="MODS"]/mets:xmlData/mods:mods[1]/mods:identifier[@type="local"][not(@invalid="yes")]',
        '//mets:mets/mets:dmdSec/mets:mdWrap[@MDTYPE="MODS"]/mets:xmlData/mods:mods[1]/mods:recordInfo/mods:recordIdentifier[@source="arcinsys.niedersachsen"]',
    ];

    #[\Override]
    public static function getIdentifier(string $xml): string
    {
        $crawler = new Crawler();
        $crawler->registerNamespace('mets', 'https://example.com');
        $crawler->addXmlContent($xml);

        /** @var string $xpath */
        foreach (self::$xpaths as $xpath) {
            $xp = $crawler->filterXPath($xpath);
            if ($xp->count() > 0) {
                if (isset($_SERVER['SERVICE_CONTEXT']) && 'digizeit' == $_SERVER['SERVICE_CONTEXT']) {
                    if (str_starts_with($xp->text(), 'PPN')) {
                        return substr($xp->text(), 3);
                    }
                } else {
                    return $xp->text();
                }
            }
        }

        return '';
    }
}
