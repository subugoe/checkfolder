<?php

namespace App\Service;

use App\Event\DeleteEvent;
use App\Event\NewWorkEvent;
use League\Flysystem\DirectoryAttributes;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\StorageAttributes;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Copying files to the target destination.
 */
final readonly class CopyService implements CopyInterface
{
    public function __construct(private FilesystemOperator $targetFilesystem, private FilesystemOperator $sourceFilesystem, private LoggerInterface $logger, private EventDispatcherInterface $eventDispatcher)
    {
    }

    /**
     * @throws FilesystemException
     */
    #[\Override]
    public function process(): void
    {
        $directories = $this->sourceFilesystem
            ->listContents('.')
            ->sortByPath()
            ->filter(static fn (StorageAttributes $attributes) => $attributes->isDir());

        /** @var DirectoryAttributes $content */
        foreach ($directories as $content) {
            /** @var DirectoryAttributes $sub */
            foreach ($this->sourceFilesystem->listContents(basename($content->path())) as $sub) {
                if (!$sub->isFile()) {
                    continue;
                }

                try {
                    $mimeType = $this->sourceFilesystem->mimeType($sub->path());
                } catch (FilesystemException) {
                    if (str_ends_with(strtolower($sub->path()), '.xml')) {
                        $mimeType = 'application/xml';
                    } else {
                        $this->logger->error(sprintf('Could not determine the mime-type of %s', $sub->path()));
                        continue;
                    }
                }

                if ($sub->isFile() && str_contains($mimeType, '/xml')) {
                    $identifier = $this->processMets($sub->path());
                    if ('' === $identifier || '0' === $identifier) {
                        continue;
                    }

                    if (!str_contains($sub->path(), '_anchor')) {
                        $this->processImages($content->path(), $identifier);
                        $this->processFulltext($content->path(), $identifier);
                    }
                    
                    $this->eventDispatcher->dispatch(new NewWorkEvent($identifier));
                    $this->logger->info(sprintf('%s => %s', $sub->path(), $identifier));
                }
            }

            try {
                $this->sourceFilesystem->deleteDirectory($content->path());
                $this->logger->debug(sprintf('Deleted directory %s', $content->path()));
            } catch (\Throwable) {
                $this->logger->error(sprintf('Failed to delete directory %s', $content->path()),
                    $content->extraMetadata());
            }
        }
    }

    private function copyFulltext(string $file, string $filename, string $id): bool
    {
        $targetDirectory = sprintf('/fulltext/%s', $id);
        try {
            $this->targetFilesystem->createDirectory($targetDirectory);

            $this->targetFilesystem->write(sprintf('%s/%s', $targetDirectory, $filename), $file);

            return true;
        } catch (FilesystemException) {
            return false;
        }
    }

    private function copyImage(string $file, string $filename, string $id): bool
    {
        $targetDirectory = sprintf('/orig/%s', $id);
        try {
            $this->targetFilesystem->createDirectory($targetDirectory);

            $this->targetFilesystem->write(sprintf('%s/%s', $targetDirectory, $filename), $file);

            return true;
        } catch (FilesystemException) {
            return false;
        }
    }

    private function copyXml(string $xml, string $target): bool
    {
        try {
            $this->targetFilesystem->write(sprintf('/mets/%s.xml', $target), $xml);

            return true;
        } catch (FilesystemException) {
            return false;
        }
    }

    private function delete(string $path): bool
    {
        try {
            $this->sourceFilesystem->delete($path);
            $this->eventDispatcher->dispatch(new DeleteEvent($path));

            return true;
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable->getMessage(), [$path]);

            return false;
        }
    }

    /**
     * @throws FilesystemException
     */
    private function processFulltext(string $path, string $identifier): void
    {
        $fulltextDirectories = [
            sprintf('%s/%s_ocr', $path, $path),
            sprintf('%s/%s_txt', $path, $path),
        ];

        foreach ($fulltextDirectories as $fulltextDirectory) {
            /** @var StorageAttributes $fulltext */
            foreach ($this->sourceFilesystem->listContents($fulltextDirectory) as $fulltext) {
                try {
                    $fulltextContent = $this->sourceFilesystem->read($fulltext->path());
                } catch (FilesystemException) {
                    $this->logger->error(sprintf('File %s not found', $fulltext->path()));
                    continue;
                }

                $isFulltextCopied = $this->copyFulltext($fulltextContent, basename($fulltext->path()), $identifier);
                if ($isFulltextCopied) {
                    $this->logger->debug(sprintf('Copied fulltext %s from %s', $fulltext->path(), $identifier), (array) $fulltext);
                    try {
                        $this->sourceFilesystem->delete($fulltext->path());
                        $this->eventDispatcher->dispatch(new DeleteEvent($fulltext->path()));
                    } catch (FilesystemException $filesystemException) {
                        $this->logger->error($filesystemException->getMessage(), (array) $fulltext);
                        continue;
                    }
                }
            }
        }
    }

    /**
     * @throws FilesystemException
     */
    private function processImages(string $path, string $identifier): void
    {
        $imageDirectory = sprintf('%s/%s_media', $path, $path);

        $this->logger->info($imageDirectory);

        /** @var StorageAttributes $image */
        foreach ($this->sourceFilesystem->listContents($imageDirectory) as $image) {
            try {
                $imageContent = $this->sourceFilesystem->read($image->path());
            } catch (FilesystemException) {
                $this->logger->error(sprintf('Image file %s not found.', $image->path()), (array) $image);
                continue;
            }

            $isImageCopied = $this->copyImage($imageContent, basename($image->path()), $identifier);
            if ($isImageCopied) {
                $this->logger->info(sprintf('Copied image %s from %s', $image->path(), $identifier), (array) $image);
                try {
                    $imagePath = $image->path();
                    $this->sourceFilesystem->delete($imagePath);
                    $this->eventDispatcher->dispatch(new DeleteEvent($imagePath));
                } catch (FilesystemException $filesystemException) {
                    $this->logger->error($filesystemException->getMessage(), (array) $image);
                    continue;
                }
            }
        }
    }

    private function processMets(string $path): string
    {
        try {
            $metsContent = $this->sourceFilesystem->read($path);
            $this->logger->info(sprintf('Starting with %s', $path));
        } catch (FilesystemException) {
            $this->logger->error(sprintf('File %s not found.', $path));

            return '';
        }

        $identifier = IdentifierService::getIdentifier($metsContent);
        if ('' === $identifier || '0' === $identifier) {
            $this->logger->error(sprintf('Could not determine identifier for %s.', $path));

            return '';
        }

        $this->logger->info(sprintf('%s => %s', $path, $identifier));

        $isXmlCopied = $this->copyXml($metsContent, $identifier);
        if ($isXmlCopied) {
            $this->logger->debug(sprintf('Copied mets file %s from %s', $path, $identifier));
        }

        return $identifier;
    }
}
