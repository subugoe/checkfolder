<?php

declare(strict_types=1);

namespace App\Service;

use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;

class DeleteCache implements DeleteCacheInterface
{
    public function __construct(private readonly FilesystemOperator $targetFilesystem)
    {
    }

    #[\Override]
    public function deleteManifests(string $id): bool
    {
        $path = '/cache/iiif/manifests/'.$id;
        try {
            $this->targetFilesystem->deleteDirectory($path);

            return true;
        } catch (FilesystemException) {
            return false;
        }
    }
}
