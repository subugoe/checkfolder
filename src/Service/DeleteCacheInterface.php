<?php

namespace App\Service;

interface DeleteCacheInterface
{
    public function deleteManifests(string $id): bool;
}
