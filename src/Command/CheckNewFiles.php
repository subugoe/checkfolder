<?php

namespace App\Command;

use App\Service\CopyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CheckNewFiles extends Command
{
    use LockableTrait;

    public function __construct(private readonly LoggerInterface $logger, private readonly CopyInterface $copyService, ?string $name = null)
    {
        parent::__construct($name);
    }

    #[\Override]
    protected function configure(): void
    {
        $this
            ->setName('app:check-new-files');
    }

    #[\Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $output->writeln('The checker is already running.');
            $this->logger->info('The checker is already running.');

            return 0;
        }

        $this->logger->info('Start hotfolding');

        $this->copyService->process();

        return 0;
    }
}
