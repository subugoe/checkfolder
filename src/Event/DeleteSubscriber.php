<?php

namespace App\Event;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final readonly class DeleteSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            DeleteEvent::class => 'onDelete',
        ];
    }

    public function onDelete(DeleteEvent $event): void
    {
        $this->logger->info(sprintf('Deleted %s.', $event->getPath()));
    }
}
