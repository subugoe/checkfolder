<?php

namespace App\Event;

use App\Service\DeleteCacheInterface;
use App\Service\NotificationInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final readonly class NewWorkSubscriber implements EventSubscriberInterface
{
    public function __construct(private LoggerInterface $logger, private NotificationInterface $notificationService, private DeleteCacheInterface $deleteCache)
    {
    }

    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            NewWorkEvent::class => 'start',
        ];
    }

    public function start(NewWorkEvent $event): void
    {
        $notification = $this->notificationService->informAboutNewWork($event->getId());
        $deletedCache = $this->deleteCache->deleteManifests($event->getId());

        if (!$notification) {
            $this->logger->error(sprintf('Failed to inform the API about %s.', $event->getId()));
        }

        if (!$deletedCache) {
            $this->logger->error(sprintf('Failed to delete the IIIF-Cache for %s.', $event->getId()));
        }
    }
}
