<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

final class DeleteEvent extends Event
{
    /**
     * @var string
     */
    public const string NAME = 'delete';

    public function __construct(private string $path)
    {
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }
}
