<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

final class NewWorkEvent extends Event
{
    /**
     * @var string
     */
    public const string NAME = 'new.work';

    public function __construct(private string $id)
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }
}
